package ua.com.limantv.fragments;


import ua.com.limantv.R;

public class AllFragment extends NotesListFragment {

    public static AllFragment newInstance() {
        return new AllFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_all;
    }

    @Override
    protected int getNumColumns() {
        return 1;
    }

    @Override
    protected int getNumItems() {
        return 10;
    }
}