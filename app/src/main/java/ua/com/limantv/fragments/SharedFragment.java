package ua.com.limantv.fragments;

public class SharedFragment extends NotesListFragment {

    public static SharedFragment newInstance() {
        return new SharedFragment();
    }

    @Override
    protected int getLayoutResId() {
        //return R.layout.fragment_shared;
        return 1;
    }

    @Override
    protected int getNumColumns() {
        return 2;
    }

    @Override
    protected int getNumItems() {
        return 10;
    }
}