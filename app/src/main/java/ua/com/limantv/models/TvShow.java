package ua.com.limantv.models;

import java.util.Date;

public class TvShow {

    private Date startTime, stopTime;
    private String tvShowName;

    public TvShow(Date startTime, Date stopTime, String tvShowName){
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.tvShowName = tvShowName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public String getTvShowName() {
        return tvShowName;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public void setTvShowName(String tvShowName) {
        this.tvShowName = tvShowName;
    }
}
