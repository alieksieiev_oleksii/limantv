package ua.com.limantv.models;

public class Channel {

    private String title;
    private String link;
    private String imageLink;
    private String tvProgramLink;

    public Channel(String title, String link, String imageLink, String tvProgramLink) {
        this.title = title;
        this.link = link;
        this.imageLink = imageLink;
        this.tvProgramLink = tvProgramLink;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String note) {
        this.link = link;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getTvProgramLink() {
        return tvProgramLink;
    }

    public void setTvProgramLink(String tvProgramLink){
        this.tvProgramLink = tvProgramLink;
    }

}
