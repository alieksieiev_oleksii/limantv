package ua.com.limantv.advertisement;

import android.content.Context;

import ua.com.limantv.R;
import ua.com.limantv.utils.Utils;


public class AdvertisementTasker {

    private static AdvertisementTasker advertisementTasker;

    public static AdvertisementTasker getInsnance() {
        if(advertisementTasker == null){
            advertisementTasker = new AdvertisementTasker();
        }
        return advertisementTasker;
    }

    public void toastMainActivity(Context context) {
        if ((AdvertisementWorker.getInstance().getToastMessageMainAct() != null) && (!AdvertisementWorker.getInstance().getToastMessageMainAct().isEmpty())){
            Utils.getInstance().showToast(context, AdvertisementWorker.getInstance().getToastMessageMainAct());
        }

    }

    public void toastVideoActivity(Context context) {
        if ((AdvertisementWorker.getInstance().getToastMessageVideoAct() != null) && (!AdvertisementWorker.getInstance().getToastMessageVideoAct().isEmpty())){
            Utils.getInstance().showToast(context, AdvertisementWorker.getInstance().getToastMessageVideoAct());
        }
    }

    public boolean bannerStartAppMainActivity(Context context) {
        if(AdvertisementWorker.getInstance().getFlagBannerMainAct()
                && AdvertisementWorker.getInstance().getAdvBannerMainAct() != null
                && AdvertisementWorker.getInstance().getAdvBannerMainAct().equals(context.getString(R.string.advertisement_startapp))){
            return true;
        } else {
            return false;
        }
    }

    public boolean bannerAdmobMainActivity(Context context) {
        if(AdvertisementWorker.getInstance().getFlagBannerMainAct()
                && AdvertisementWorker.getInstance().getAdvBannerMainAct() != null
                && AdvertisementWorker.getInstance().getAdvBannerMainAct().equals(context.getString(R.string.advertisement_admob))){
            return true;
        } else {
            return false;
        }
    }

    public boolean dialog() {
        if(AdvertisementWorker.getInstance().getFlagShowDialog()
                && AdvertisementWorker.getInstance().getDialogTitle() != null
                && AdvertisementWorker.getInstance().getDialogMessage() != null
                && AdvertisementWorker.getInstance().getDialogLink() != null){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageAdmobBackMainAct(Context context) {
        if(AdvertisementWorker.getInstance().getFlagPageBackMainAct()
                && AdvertisementWorker.getInstance().getAdvPageBackMainAct() != null
                && AdvertisementWorker.getInstance().getAdvPageBackMainAct().equals(context.getString(R.string.advertisement_admob))){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageStartAppBackMainAct(Context context) {
        if(AdvertisementWorker.getInstance().getFlagPageBackMainAct()
                && AdvertisementWorker.getInstance().getAdvPageBackMainAct() != null
                && AdvertisementWorker.getInstance().getAdvPageBackMainAct().equals(context.getString(R.string.advertisement_startapp))){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageAdmobStartApplication(Context context) {
        if(AdvertisementWorker.getInstance().getFlagPageStartApp()
                && AdvertisementWorker.getInstance().getAdvPageStartApp() != null
                && AdvertisementWorker.getInstance().getAdvPageStartApp().equals(context.getString(R.string.advertisement_admob))){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageStartAppStartApplication(Context context) {
        if(AdvertisementWorker.getInstance().getFlagPageStartApp()
                && AdvertisementWorker.getInstance().getAdvPageStartApp() != null
                && AdvertisementWorker.getInstance().getAdvPageStartApp().equals(context.getString(R.string.advertisement_startapp))){
            return true;
        } else {
            return false;
        }
    }

    /****************************** Video Activity ******************************/

    public boolean bannerAdmobVideoActivity(Context context) {
        if(AdvertisementWorker.getInstance().getFlagBannerVideoAct()
                && AdvertisementWorker.getInstance().getAdvBannerVideoAct() != null
                && AdvertisementWorker.getInstance().getAdvBannerVideoAct().equals(context.getString(R.string.advertisement_admob))){
            return true;
        } else {
            return false;
        }
    }

    public boolean bannerStartAppVideoActivity(Context context) {
        if(AdvertisementWorker.getInstance().getFlagBannerVideoAct()
                && AdvertisementWorker.getInstance().getAdvBannerVideoAct() != null
                && AdvertisementWorker.getInstance().getAdvBannerVideoAct().equals(context.getString(R.string.advertisement_startapp))){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageAdmobBackVideoAct(Context context) {
        if(AdvertisementWorker.getInstance().getFlagPageBackVideoAct()
                && AdvertisementWorker.getInstance().getAdvPageBackVideoAct() != null
                && AdvertisementWorker.getInstance().getAdvPageBackVideoAct().equals(context.getString(R.string.advertisement_admob))){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageStartAppBackVideoAct(Context context) {
        if(AdvertisementWorker.getInstance().getFlagPageBackVideoAct()
                && AdvertisementWorker.getInstance().getAdvPageBackVideoAct() != null
                && AdvertisementWorker.getInstance().getAdvPageBackVideoAct().equals(context.getString(R.string.advertisement_startapp))){
            return true;
        } else {
            return false;
        }
    }

}