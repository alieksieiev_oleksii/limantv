package ua.com.limantv.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

import ua.com.limantv.R;
import ua.com.limantv.instance.YouTubeInstance;
import ua.com.limantv.utils.Utils;

public class YouTubeActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{

    private YouTubePlayerView youTubePlayerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_youtube);
        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubePlayerView.initialize(YouTubeInstance.getInstance().getYoutube_api_key(), this);
    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Utils.getInstance().showToast(getApplicationContext(), "Errot initialize youtube player");
    }

    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                @Override
                public void onLoading() {

                }

                @Override
                public void onLoaded(String s) {

                }

                @Override
                public void onAdStarted() {

                }

                @Override
                public void onVideoStarted() {

                }

                @Override
                public void onVideoEnded() {
                    startActivity(new Intent(YouTubeActivity.this, VideoActivity.class));
                    finish();
                }

                @Override
                public void onError(YouTubePlayer.ErrorReason errorReason) {

                }
            });
            youTubePlayer.setFullscreen(true);
            youTubePlayer.setShowFullscreenButton(false);
            youTubePlayer.loadVideo(YouTubeInstance.getInstance().getYoutube_video_key(), 0);
        }
    }
}
