package ua.com.limantv.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;


import com.google.android.youtube.player.YouTubePlayer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;

import cn.pedant.SweetAlert.SweetAlertDialog;
import ua.com.limantv.R;
import ua.com.limantv.advertisement.AdvertisementTasker;
import ua.com.limantv.advertisement.AdvertisementWorker;
import ua.com.limantv.instance.ChannelsWorker;
import ua.com.limantv.instance.YouTubeInstance;
import ua.com.limantv.utils.Utils;


public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT;
    private boolean app_status_publication;
    private Button btn_tv;
    private boolean dialog_is_showed;
    private SweetAlertDialog sweetAlertDialog;
    private SweetAlertDialog sweetAlertDialogInfo;
    private String telephone = "";
    private String personal_account = "";
    private String tech_support = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        trimCache();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#015086")));
        if (Utils.getInstance().checkNetworkConnection(getApplicationContext())) {
            new AsyncJsonParser().execute();
        } else {
            Utils.getInstance().showToast(getApplicationContext(), getString(R.string.toast_message_net_conn_error));
        }
        btn_tv = (Button) findViewById(R.id.btn_tv);
        btn_tv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(YouTubeInstance.getInstance().getBln_show_at_start()){
                    startActivity(new Intent(SplashActivity.this, YouTubeActivity.class));
                }else {
                    startActivity(new Intent(SplashActivity.this, VideoActivity.class));
                }

            }
        });
        btn_tv.requestFocus();


        //String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        //Utils.getInstance().printToLogs(fingerprints[0]);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.info:
                this.sweetAlertDialogInfo = new SweetAlertDialog(SplashActivity.this, SweetAlertDialog.NORMAL_TYPE)
                        .setTitleText("Информация")
                        .setContentText("Rubin-TV - Современное цифровое IPTV телевидение.\n" +
                                "Телевидение + онлайн телепрограмма, 120 телеканалов.\n" +
                                "Детские,  семейные, новостные, развлекательные каналы.\n" +
                                "Каналы транслируются в цифровом качестве, в том числе  HD.\n" +
                                "Просмотр каналов возможен только из сети Rubintelecom.\n" +
                                "\n"+
                                "Разработка мобильных приложений Live-TV Service (live-tv.net.ua)");
                this.sweetAlertDialogInfo.show();
                break;
            case R.id.refresh:
                if (Utils.getInstance().checkNetworkConnection(getApplicationContext())) {
                    trimCache();
                    new AsyncJsonParser().execute();
                }
                break;
            case R.id.lichniy_cabinet:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(personal_account)));
                break;
            case R.id.tech_support:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(tech_support)));
                break;
            case R.id.telophone:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("tel", telephone, null)));
                break;
            /*case R.id.vk:
                VKSdk.login(this, scopes);
                break;*/
            case R.id.exit:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    class AsyncJsonParser extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getInstance().printToLogs("onPreExecute");
        }

        @Override
        protected Void doInBackground(Void... voids) {
            StringBuffer stringBufferLocal;
            URL url;
            BufferedReader bufferedReader;
            String inputLine;
            JSONParser jsonParser;
            JSONObject jsonObject;
            JSONArray jsonArray;
            int count;
            try {
                stringBufferLocal = new StringBuffer();
                url = new URL(getString(R.string.db_path));
                bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
                while ((inputLine = bufferedReader.readLine()) != null) {
                    stringBufferLocal.append(inputLine);
                }
                bufferedReader.close();
                jsonParser = new JSONParser();
                jsonObject = (JSONObject) jsonParser.parse(stringBufferLocal.toString());
                if((jsonObject.get(getString(R.string.app_status)).toString()).equals(getString(R.string.app_status_publication))){
                    app_status_publication = true;
                }
                /*set toast messages*/
                jsonArray = (JSONArray) jsonObject.get(getString(R.string.toast_message));
                AdvertisementWorker.getInstance().setToastMessage(jsonArray.get(0).toString(), jsonArray.get(1).toString());
                /*set advertisement info*/
                jsonArray = (JSONArray) jsonObject.get(getString(R.string.advertisement_info));
                Utils.getInstance().printToLogs(jsonArray.toString());
                AdvertisementWorker.getInstance().setAdvertisementInfo(jsonArray.get(0).toString(), jsonArray.get(1).toString(),
                        jsonArray.get(2).toString(), jsonArray.get(3).toString(),
                        jsonArray.get(4).toString(), jsonArray.get(5).toString(),
                        jsonArray.get(6).toString(), jsonArray.get(7).toString(),
                        jsonArray.get(8).toString(), jsonArray.get(9).toString());
                /*set dialog*/
                jsonArray = (JSONArray) jsonObject.get(getString(R.string.dialog));
                AdvertisementWorker.getInstance().setDialog(jsonArray.get(0).toString(), jsonArray.get(1).toString(), jsonArray.get(2).toString(), jsonArray.get(3).toString());
                /*set advertisement id's*/
                jsonArray = (JSONArray) jsonObject.get(getString(R.string.advertisement_id));
                AdvertisementWorker.getInstance().setAdvertisementIds(jsonArray.get(0).toString(), jsonArray.get(1).toString(), jsonArray.get(2).toString(), jsonArray.get(3).toString(), jsonArray.get(4).toString());
                /*set channels info*/
                count = Integer.parseInt(jsonObject.get("count").toString());
                telephone = jsonObject.get("telephone").toString();
                personal_account = jsonObject.get("personal_account").toString();
                tech_support = jsonObject.get("tech_support").toString();

                jsonArray = (JSONArray) jsonObject.get("youtube_info");
                YouTubeInstance.getInstance().setYouTubeInfo(Boolean.parseBoolean(jsonArray.get(0).toString()),
                        Boolean.parseBoolean(jsonArray.get(1).toString()),
                                jsonArray.get(2).toString(),
                                jsonArray.get(3).toString(),
                                Integer.parseInt(jsonArray.get(4).toString()));

                ChannelsWorker.reloadChannelWorker(); //clean channel worker
                for (int i = 1; i <= count; i++) {
                    jsonArray = (JSONArray) jsonObject.get(String.valueOf(i));
                    ChannelsWorker.getInstance().addChannelBean(jsonArray.get(0).toString(), jsonArray.get(1).toString(), jsonArray.get(2).toString(), jsonArray.get(3).toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.getInstance().printToLogs("onPostExecute");
            if (ChannelsWorker.getInstance().getCountChannelsBeans() != 0) {
                if (SplashActivity.this.app_status_publication) {
                    Utils.getInstance().showToast(SplashActivity.this.getApplication(), SplashActivity.this.getString(R.string.toast_publishing));
                    return;
                }
                AdvertisementTasker.getInsnance().toastMainActivity(SplashActivity.this.getApplicationContext());
                if (AdvertisementTasker.getInsnance().dialog() && !SplashActivity.this.dialog_is_showed) {
                    sweetAlertDialog = new SweetAlertDialog(SplashActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText(AdvertisementWorker.getInstance()
                                    .getDialogTitle()).setContentText(AdvertisementWorker.getInstance().getDialogMessage())
                            .setCancelText(SplashActivity.this.getString(R.string.dialod_no))
                            .setConfirmText(SplashActivity.this.getString(R.string.dialod_yes))
                            .showCancelButton(false)
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            if(sweetAlertDialog != null && sweetAlertDialog.isShowing()) sweetAlertDialog.dismiss();
                        }
                    })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    if(sweetAlertDialog != null && sweetAlertDialog.isShowing()) sweetAlertDialog.dismiss();
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AdvertisementWorker.getInstance().getDialogLink())));
                                }
                            });
                    sweetAlertDialog.show();
                    dialog_is_showed = true;
                }
            }
        }
    }

    public void trimCache() {
        try {
            File dir = getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                if (!deleteDir(new File(dir, file))) {
                    return false;
                }
            }
        }
        return dir.delete();
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 82){
            if(event.getAction() == KeyEvent.ACTION_DOWN){
          //      getSupportActionBar().openOptionsMenu();
          //      openOptionsMenu();
           //     getWindow().openPanel(Window.FEATURE_OPTIONS_PANEL, event);
            }
        }
        //Utils.getInstance().showToast(getApplicationContext(), String.valueOf((event.getKeyCode())));
        return super.dispatchKeyEvent(event);
    }
}
