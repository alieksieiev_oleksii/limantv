package ua.com.limantv.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import ua.com.limantv.R;
import ua.com.limantv.adapter.ImageAdapterGrid;
import ua.com.limantv.adapter.MyAdapter;
import ua.com.limantv.advertisement.AdvertisementTasker;
import ua.com.limantv.instance.ChannelsWorker;
import ua.com.limantv.instance.YouTubeInstance;
import ua.com.limantv.models.TvShow;
import ua.com.limantv.utils.Utils;

public class VideoActivity extends Activity {

    //private IjkVideoView mVideoView;
    private RelativeLayout mainLayout;
    private int position = 0;
    private boolean mBackPressed;
    //private Button button;
    private ListView lvTvShows;
    private String curTvShow = "";
    private int curTvShowPos;
    private boolean foundStatus = false;
    private GridView gridView;
    private int  curVol = 0, columnWidth, mPhotoSize, mPhotoSpacing;
    private ImageAdapterGrid imageAdapter;
    private ProgressBar progressBar;
    private boolean isViewsShow;
    private AudioManager audioManager;
    private SharedPreferences sharedPreferences;
    private int countPosViews = 0;
    private boolean isTimiFinished = false;
    private VideoView videoView;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_video);
        videoView = (VideoView) findViewById(R.id.video_view);
        mainLayout = (RelativeLayout)findViewById(R.id.rl);
        //button = (Button) findViewById(R.id.button);
        gridView = (GridView) findViewById(R.id.gridview);
        lvTvShows = (ListView) findViewById(R.id.lvTvShows);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        position = getIntent().getIntExtra(getString(R.string.intent), 0);

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        curVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        //button.setVisibility(View.INVISIBLE);
        lvTvShows.setVisibility(View.INVISIBLE);
        gridView.setVisibility(View.INVISIBLE);
        String tvProgramLink = ChannelsWorker.getInstance().getChannelBean(position).getTvProgramLink();
        if(tvProgramLink != null && !tvProgramLink.isEmpty() && (tvProgramLink.length() >1)){
            new Parser().execute(tvProgramLink);
        }

        // init player
        //IjkMediaPlayer.loadLibrariesOnce(null);
        //IjkMediaPlayer.native_profileBegin("libijkplayer.so");

        sharedPreferences = getSharedPreferences("position", MODE_PRIVATE);
        loadPreferences();
        videoView.setVideoPath(ChannelsWorker.getInstance().getChannelBean(position).getLink());

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                hideUI();
                progressBar.setVisibility(View.INVISIBLE);
                videoView.start();
            }
        });

        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(gridView.getVisibility() == View.VISIBLE) {
                    //mVideoView.reToggleAspectRatio();
                    //button.setVisibility(View.INVISIBLE);
                    lvTvShows.setVisibility(View.INVISIBLE);
                    gridView.setVisibility(View.INVISIBLE);
                    isViewsShow = false;
                    //hideUI();
                }
                else {
                    //mVideoView.reToggleAspectRatio();
                    //button.setVisibility(View.VISIBLE);
                    lvTvShows.setVisibility(View.VISIBLE);
                    gridView.setVisibility(View.VISIBLE);
                    isViewsShow = true;
                    //showUI();
                }
                return false;
            }
        });

        /*
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVideoView.toggleAspectRatio();
            }
        });
        */

        mPhotoSize = getResources().getDimensionPixelSize(R.dimen.photo_size);
        mPhotoSpacing = getResources().getDimensionPixelSize(R.dimen.photo_spacing);
        imageAdapter = new ImageAdapterGrid(getApplicationContext());

        gridView.setNumColumns(1);
        gridView.setAdapter(imageAdapter);
        gridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (imageAdapter.getNumColumns() == 0) {
                    final int numColumns = (int) Math.floor(gridView.getWidth() / (mPhotoSize + mPhotoSpacing));
                    if (numColumns > 0) {
                        columnWidth = (gridView.getWidth() / numColumns) - mPhotoSpacing;
                        imageAdapter.setNumColumns(numColumns);
                        imageAdapter.setItemHeight(columnWidth);
                    }
                }
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                position = i;
                progressBar.setVisibility(View.VISIBLE);
                String tvProgramLink = ChannelsWorker.getInstance().getChannelBean(i).getTvProgramLink();
                if(tvProgramLink != null && !tvProgramLink.isEmpty() && (tvProgramLink.length() >1)){
                    new Parser().execute(tvProgramLink);
                }

                videoView.setVideoPath(ChannelsWorker.getInstance().getChannelBean(i).getLink());
                videoView.start();
                savePreferences();
                Utils.getInstance().showToast(getApplicationContext(), ChannelsWorker.getInstance().getChannelBean(i).getTitle());
                if(isTimiFinished){
                    startActivity(new Intent(VideoActivity.this, YouTubeActivity.class));
                    finish();
                }
            }
        });

        AdvertisementTasker.getInsnance().toastVideoActivity(getApplicationContext());

        if(YouTubeInstance.getInstance().getBln_show_with_delay()){
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    isTimiFinished = true;
                }
            }, YouTubeInstance.getInstance().getYoutube_video_delay());
        }
    }

    private void hideUI() {
        if (Build.VERSION.SDK_INT == 14 || Build.VERSION.SDK_INT == 15) {
            mainLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
            isViewsShow = false;
        } else if (Build.VERSION.SDK_INT >= 16) {
            mainLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_FULLSCREEN);
            isViewsShow = false;
        }
    }

    private void showUI() {
        isViewsShow = true;
        mainLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE );

    }


    @Override
    protected void onPause() {
        if(videoView != null) videoView.stopPlayback();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);
    }
/*
    @Override
    protected void onStop() {
        super.onStop();
        if (mBackPressed || !mVideoView.isBackgroundPlayEnabled()) {
            mVideoView.stopPlayback();
            mVideoView.release(true);
            mVideoView.stopBackgroundPlay();
        } else {
            mVideoView.enterBackground();
        }
        IjkMediaPlayer.native_profileEnd();
    }
*/
    @Override
    public void onBackPressed() {
        mBackPressed = true;
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if(videoView != null )videoView.stopPlayback();
        progressBar.setVisibility(View.INVISIBLE);
        super.onDestroy();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
      /*  Utils.getInstance().showToast(getApplicationContext(), String.valueOf(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)));
        if(event.getKeyCode() == 19){
            if(event.getAction() == KeyEvent.ACTION_DOWN){
                curVol = curVol + 10;
                if(curVol >= audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)){
                    curVol = 255;
                    Utils.getInstance().showToast(getApplicationContext(), "Max volume");
                }
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, curVol, 0);
            }
            return super.dispatchKeyEvent(event);
        }
        if(event.getKeyCode() == 20){
            if(event.getAction() == KeyEvent.ACTION_DOWN){
                curVol = curVol - 10;
                if(curVol <= 0){
                    curVol = 0;
                    Utils.getInstance().showToast(getApplicationContext(), "Min volume");
                }
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, curVol, 0);
            }
            return super.dispatchKeyEvent(event);
        }*/
        Utils.getInstance().printToLogs(String.valueOf(event.getKeyCode()));

        if(gridView.getVisibility() == View.INVISIBLE && lvTvShows.getVisibility() == View.INVISIBLE && event.getKeyCode() == 19){ //Up
            if(event.getAction() == KeyEvent.ACTION_DOWN){
                Utils.getInstance().printToLogs("Up");
                ++position;
                if(position >= (ChannelsWorker.getInstance().getCountChannelsBeans() - 1)) position = ChannelsWorker.getInstance().getCountChannelsBeans() - 1;
                final String title = ChannelsWorker.getInstance().getChannelBean(position).getTitle();
                String link = ChannelsWorker.getInstance().getChannelBean(position).getLink();
                String imageLink = ChannelsWorker.getInstance().getChannelBean(position).getImageLink();
                final String tvProgramLink = ChannelsWorker.getInstance().getChannelBean(position).getTvProgramLink();
                //showProgressDoalog();
                if(tvProgramLink != null && !tvProgramLink.isEmpty() && (tvProgramLink.length() >1)){
                    new Parser().execute(tvProgramLink);
                }
                //Utils.getInstance().showToast(getApplicationContext(), title);
                videoView.setVideoPath(ChannelsWorker.getInstance().getChannelBean(position).getLink());
                videoView.start();
                Utils.getInstance().showToast(getApplicationContext(), ChannelsWorker.getInstance().getChannelBean(position).getTitle());
                savePreferences();
                return super.dispatchKeyEvent(event);
            }
        }
        if(gridView.getVisibility() == View.INVISIBLE && lvTvShows.getVisibility() == View.INVISIBLE &&  event.getKeyCode() == 20){ //Down
            if(event.getAction() == KeyEvent.ACTION_DOWN) {
                Utils.getInstance().printToLogs("Down");
                --position;
                if(position <= 0) position = 0;
                //if
                final String title = ChannelsWorker.getInstance().getChannelBean(position).getTitle();
                String link = ChannelsWorker.getInstance().getChannelBean(position).getLink();
                String imageLink = ChannelsWorker.getInstance().getChannelBean(position).getImageLink();
                final String tvProgramLink = ChannelsWorker.getInstance().getChannelBean(position).getTvProgramLink();
                //showProgressDoalog();
                if(tvProgramLink != null && !tvProgramLink.isEmpty() && (tvProgramLink.length() >1)){
                    new Parser().execute(tvProgramLink);
                }
                //Utils.getInstance().showToast(getApplicationContext(), title);
                videoView.setVideoPath(ChannelsWorker.getInstance().getChannelBean(position).getLink());
                videoView.start();
                Utils.getInstance().showToast(getApplicationContext(), ChannelsWorker.getInstance().getChannelBean(position).getTitle());
                savePreferences();
                return super.dispatchKeyEvent(event);
            }
        }

        if(event.getKeyCode() == 82){ //Menu
            if(event.getAction() == KeyEvent.ACTION_DOWN){
                if(countPosViews == 3) countPosViews = 0;
                switch (countPosViews){
                    case 0:
                        lvTvShows.setVisibility(View.INVISIBLE);
                        gridView.setVisibility(View.VISIBLE);
                        gridView.requestFocus();
                        //button.setVisibility(View.INVISIBLE);
                        isViewsShow = true;
                        countPosViews++;
                        break;
                    case 1:
                        lvTvShows.setVisibility(View.VISIBLE);
                        lvTvShows.requestFocus();
                        gridView.setVisibility(View.INVISIBLE);
                        //button.setVisibility(View.INVISIBLE);
                        isViewsShow = true;
                        countPosViews++;
                        break;
                    case 2:
                        lvTvShows.setVisibility(View.INVISIBLE);
                        gridView.setVisibility(View.INVISIBLE);
                        //button.setVisibility(View.INVISIBLE);
                        isViewsShow = false;
                        countPosViews++;
                        break;

                }
            }
        }

        if(event.getKeyCode() == 21 || event.getKeyCode() == 22){
            if(event.getAction() == KeyEvent.ACTION_DOWN){
                //mVideoView.toggleAspectRatio();
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public class Parser extends AsyncTask<String, Void, ArrayList<String>> {

        private TagNode tagNode;
        private HtmlCleaner cleaner;
        private SimpleDateFormat sdfTvProgram;
        private Date startTime, stopTime, currentTime;
        private String tvShowName;

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<TvShow> tvShows = new ArrayList<>();
            ArrayList<String> tvShows2 = new ArrayList<>();
            cleaner = new HtmlCleaner();
            try {
                tagNode = cleaner.clean(new URL(params[0]));
            } catch (IOException e) {
                e.printStackTrace();
            }
            sdfTvProgram = new SimpleDateFormat("yyyyMMddHHmm");
            Utils.getInstance().printToLogs(params[0]);
            TagNode tnProgramme[] = tagNode.getElementsByName("programme", true);
            TagNode tnName[] = tagNode.getElementsByName("name", true);
            for(int i=0; i<tnProgramme.length; i++){
                try {
                    startTime = sdfTvProgram.parse(tnProgramme[i].getAttributeByName("start").substring(0,12));
                    stopTime = sdfTvProgram.parse(tnProgramme[i].getAttributeByName("stop").substring(0,12));
                    tvShowName = tnName[i].getText().toString().replace("&quot;", "\"");
                    tvShows.add(new TvShow(startTime, stopTime, tvShowName));
                    tvShows2.add(getDayOfWeekRus(startTime) + " " + getHourFromDate(startTime) + "." + getMinFromDate(startTime) + " - "
                            + getHourFromDate(stopTime) + "." + getMinFromDate(stopTime) + "  "+tvShowName);
                    //Utils.getInstance().printToLogs(getHourFromDate(startTime) + " " + getDayOfWeekRus(startTime) + " " + startTime.toString() + " " + stopTime.toString() + " " + tvShowName);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                try {
                    //Utils.getInstance().printToLogs("Start " + startTime.toString());
                    //Utils.getInstance().printToLogs("Stop " + stopTime.toString());
                    //Utils.getInstance().printToLogs("Cur " + getKyivTime().toString());
                    //Utils.getInstance().printToLogs(String.valueOf(isTimeBetweenTwoTime(startTime, stopTime, getKyivTime())));
                    if(isTimeBetweenTwoTime(startTime, stopTime, getKyivTime())){
                        Utils.getInstance().printToLogs("Start " + startTime.toString());
                        Utils.getInstance().printToLogs("Stop " + stopTime.toString());
                        Utils.getInstance().printToLogs("Cur " + getKyivTime().toString());
                        curTvShow = tnName[i].getText().toString().replace("&quot;", "\"");
                        curTvShowPos = i;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            return tvShows2;
        }

        @Override
        protected void onPostExecute(ArrayList s) {
            super.onPostExecute(s);
            //lvTvShows.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.my_list_item, s));
            lvTvShows.setAdapter(new MyAdapter(getApplicationContext(), s, curTvShowPos));
            lvTvShows.setSelection(curTvShowPos);
            //Utils.getInstance().showToast(getApplicationContext(), curTvShow);
        }

        Date getKyivTime(){
            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            //long gmtTime = calendar.getTime().getTime();
            //long timezoneAlteredTime = gmtTime + TimeZone.getTimeZone("Europe/Kiev").getRawOffset();
            //Calendar calendarKyiv = Calendar.getInstance(TimeZone.getTimeZone("Europe/Kiev"));
            //calendarKyiv.setTimeInMillis(timezoneAlteredTime);
            //return calendarKyiv.getTime();
            return calendar.getTime();
        }


        String getDayOfWeekRus(Date date){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int intdate = calendar.get(Calendar.DAY_OF_WEEK);
            String weekDayRus = "";
            switch (intdate){
                case 1: weekDayRus = "Вс";
                    break;
                case 2: weekDayRus = "Пн";
                    break;
                case 3: weekDayRus = "Вт";
                    break;
                case 4: weekDayRus = "Ср";
                    break;
                case 5: weekDayRus = "Чт";
                    break;
                case 6: weekDayRus = "Пт";
                    break;
                case 7: weekDayRus = "Сб";
                    break;

            }
            return weekDayRus;
        }

        int getHourFromDate(Date date){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.get(Calendar.HOUR_OF_DAY);
        }

        String getMinFromDate(Date date){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            if((calendar.get(Calendar.MINUTE)) == 0){
                return "00";
            } else {
                return String.valueOf(calendar.get(Calendar.MINUTE));
            }
        }

        public boolean isTimeBetweenTwoTime(Date initialTime, Date finalTime, Date currentTime) throws ParseException {
            try {
                boolean valid = false;
                //Start Time
                java.util.Date inTime = initialTime;
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(inTime);

                //Current Time
                java.util.Date checkTime = currentTime;
                Calendar calendar3 = Calendar.getInstance();
                calendar3.setTime(checkTime);

                //End Time
                java.util.Date finTime = finalTime;
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(finTime);

                if (finalTime.compareTo(initialTime) < 0) {
                    calendar2.add(Calendar.DATE, 1);
                    calendar3.add(Calendar.DATE, 1);
                }

                java.util.Date actualTime = calendar3.getTime();
                if ((actualTime.after(calendar1.getTime()) || actualTime.compareTo(calendar1.getTime()) == 0)
                        && actualTime.before(calendar2.getTime())) {
                    valid = true;
                }
                return valid;
            } catch (IllegalArgumentException ex) {
                Utils.getInstance().showToast(getApplicationContext(), "Error with parse time");
            }
            return false;
        }

    }

    private void savePreferences() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("position", position);
        editor.commit();
    }

    private void loadPreferences() {
        position = sharedPreferences.getInt("position", 0);
    }
}