package ua.com.limantv.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ua.com.limantv.R;


public class MyAdapter extends BaseAdapter {

    private ArrayList<String> alValues;
    private int itnCurTvShow;
    private Context context;
    private LayoutInflater inflater;

    public MyAdapter(Context context, ArrayList<String> alValues, int itnCurTvShow){
        this.context = context;
        this.alValues = alValues;
        this.itnCurTvShow = itnCurTvShow;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return alValues.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = inflater.inflate(R.layout.my_list_item, null);
        TextView textView = (TextView)rootView.findViewById(R.id.tv_red);
        if(position == itnCurTvShow) {
            textView.setText(alValues.get(position));
            textView.setTypeface(null, Typeface.BOLD);
            textView.setBackgroundColor(ContextCompat.getColor(context, R.color.ijk_color_blue_300));
        }else {
            if(alValues.get(position).startsWith("Пн")){
                textView.setTextColor(ContextCompat.getColor(context, R.color.color_1));
            } else if(alValues.get(position).startsWith("Вт")){
                textView.setTextColor(ContextCompat.getColor(context, R.color.color_2));
            } else if(alValues.get(position).startsWith("Ср")){
                textView.setTextColor(ContextCompat.getColor(context, R.color.color_1));
            } else if(alValues.get(position).startsWith("Чт")){
                textView.setTextColor(ContextCompat.getColor(context, R.color.color_2));
            } else if(alValues.get(position).startsWith("Пт")){
                textView.setTextColor(ContextCompat.getColor(context, R.color.color_1));
            } else if(alValues.get(position).startsWith("Сб")){
                textView.setTextColor(ContextCompat.getColor(context, R.color.color_2));
            } else if(alValues.get(position).startsWith("Вс")){
                textView.setTextColor(ContextCompat.getColor(context, R.color.color_1));
            }

            textView.setText(alValues.get(position));
        }
        return rootView;
    }
}
