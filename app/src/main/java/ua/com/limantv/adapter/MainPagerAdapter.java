package ua.com.limantv.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ua.com.limantv.R;
import ua.com.limantv.fragments.AllFragment;
import ua.com.limantv.fragments.SharedFragment;


public class MainPagerAdapter extends FragmentPagerAdapter {

    public static final int NUM_ITEMS = 1;
    public static final int ALL_POS = 0;
    public static final int SHARED_POS = 1;

    private Context context;

    public MainPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case ALL_POS:
                return AllFragment.newInstance();
            case SHARED_POS:
                return SharedFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case ALL_POS:
                return context.getString(R.string.all);
            case SHARED_POS:
                return context.getString(R.string.shared);
            default:
                return "";
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

}
