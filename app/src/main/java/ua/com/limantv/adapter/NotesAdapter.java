package ua.com.limantv.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import cn.pedant.SweetAlert.SweetAlertDialog;
import ua.com.limantv.R;
import ua.com.limantv.activity.VideoActivity;
import ua.com.limantv.instance.ChannelsWorker;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    private Context context;

    public NotesAdapter(Context context, int numNotes) {
        this.context = context;
    }

    @Override
    public NotesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_note, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String title = ChannelsWorker.getInstance().getChannelBean(position).getTitle();
        String link = ChannelsWorker.getInstance().getChannelBean(position).getLink();
        String imageLink = ChannelsWorker.getInstance().getChannelBean(position).getImageLink();
        String tvProgramLink = ChannelsWorker.getInstance().getChannelBean(position).getTvProgramLink();
        int color = getRandomColor(context);

        // Set text
        holder.titleTextView.setText(title);
        //holder.tvProgram.setText(link);
        if (tvProgramLink != null && !tvProgramLink.isEmpty() && (tvProgramLink.length() > 1)) {
            //new Parser(holder.tvProgram).execute(tvProgramLink);
        }

        Picasso.with(context).load(context.getString(R.string.string_path_to_images) + imageLink).into(holder.infoImageView);

        // Set visibilities
        holder.titleTextView.setVisibility(TextUtils.isEmpty(title) ? View.GONE : View.VISIBLE);
        //holder.tvProgram.setVisibility(TextUtils.isEmpty() ? View.GONE : View.VISIBLE);

        // Set background color
        ((CardView) holder.itemView).setCardBackgroundColor(color);
    }

    private static int getRandomColor(Context context) {
        int[] colors;
        if (Math.random() >= 0.6) {
            colors = context.getResources().getIntArray(R.array.note_accent_colors);
        } else {
            colors = context.getResources().getIntArray(R.array.note_neutral_colors);
        }
        return colors[((int) (Math.random() * colors.length))];
    }

    @Override
    public int getItemCount() {
        return ChannelsWorker.getInstance().getCountChannelsBeans();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTextView;
        public TextView tvProgram;
        public ImageView infoImageView;
        private Context context;
        SweetAlertDialog sweetAlertDialog;

        public ViewHolder(final View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context = itemView.getContext();
                    if (!ChannelsWorker.getInstance().getChannelBean(getAdapterPosition()).getLink().contains("m3u8")) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ChannelsWorker.getInstance().getChannelBean(getAdapterPosition()).getLink())));
                    } else {
                        Intent intent = new Intent(context, VideoActivity.class);
                        intent.putExtra(context.getString(R.string.intent), getAdapterPosition());
                        context.startActivity(intent);
                    }
                }
            });
            titleTextView = (TextView) itemView.findViewById(R.id.note_title);
            tvProgram = (TextView) itemView.findViewById(R.id.tv_program);
            infoImageView = (ImageView) itemView.findViewById(R.id.note_info_image);
        }
                    /*
                    if (isAppInstalled(context, context.getString(R.string.vlc_pacckge_name))){
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        //i.setPackage(context.getString(R.string.vlc_pacckge_name));
                        //i.setPackage("org.videolan.vlc.betav7neon");
                        i.setDataAndTypeAndNormalize(Uri.parse(ChannelsWorker.getInstance().getChannelBean(getAdapterPosition()).getLink()), "video/*");
                        Utils.getInstance().printToLogs("Channel " + ChannelsWorker.getInstance().getChannelBean(getAdapterPosition()).getLink());
                        context.startActivity(i);
                    }else {
                       sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Необходим VLC плеер")
                                .setContentText("Установить?")
                                .setCancelText(context.getString(R.string.dialod_no))
                                .setConfirmText(context.getString(R.string.dialod_yes))
                                .showCancelButton(false)
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        if(sweetAlertDialog != null && sweetAlertDialog.isShowing()) sweetAlertDialog.dismiss();
                                    }
                                })
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        if(sweetAlertDialog != null && sweetAlertDialog.isShowing()) sweetAlertDialog.dismiss();
                                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=org.videolan.vlc")));
                                    }
                                });
                        sweetAlertDialog.show();

                    }
                    */

        //titleTextView = (TextView) itemView.findViewById(R.id.note_title);
        //tvProgram = (TextView) itemView.findViewById(R.id.tv_program);
        //infoImageView = (ImageView) itemView.findViewById(R.id.note_info_image);
        //}
        //}

        public static boolean isAppInstalled(Context context, String packageName) {
            try {
                context.getPackageManager().getApplicationInfo(packageName, 0);
                return true;
            } catch (PackageManager.NameNotFoundException e) {
                return false;
            }
        }


    }
}