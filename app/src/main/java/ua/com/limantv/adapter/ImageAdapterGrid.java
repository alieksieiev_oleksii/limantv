package ua.com.limantv.adapter;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ua.com.limantv.R;
import ua.com.limantv.instance.ChannelsWorker;


/**
 * Created by Alex on 3/16/2017.
 */

public class ImageAdapterGrid extends BaseAdapter {

    private Context context;
    private LruCache<String, Bitmap> mMemoryCache;
    private LayoutInflater mInflater;
    private int mItemHeight = 0;
    private int mNumColumns = 0;
    private RelativeLayout.LayoutParams mImageViewLayoutParams;

    public ImageAdapterGrid(Context _context) {
        context = _context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImageViewLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        // Get memory class of this device, exceeding this amount will throw an OutOfMemory exception.
        final int memClass = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
        final int cacheSize = 1024 * 1024 * memClass / 8; // Use 1/8th of the available memory for this memory cache.

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {

            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount(); // The cache size will be measured in bytes rather than number of items.
            }
        };
    }


    public void setItemHeight(int height) {
        if (height == mItemHeight) {
            return;
        }
        mItemHeight = height;
        mImageViewLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mItemHeight);
        notifyDataSetChanged();
    }

    public int getCount() {
        return ChannelsWorker.getInstance().getCountChannelsBeans();
    }

    public void setNumColumns(int numColumns) {
        mNumColumns = numColumns;
    }

    public int getNumColumns() {
        return mNumColumns;
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            convertView = mInflater.inflate(R.layout.myitem, null);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.cover);
        TextView title = (TextView) convertView.findViewById(R.id.title);

        imageView.setLayoutParams(mImageViewLayoutParams);

        // Check the height matches our calculated column width
        if (imageView.getLayoutParams().height != mItemHeight) {
            imageView.setLayoutParams(mImageViewLayoutParams);
        }

        Picasso.with(context).load(context.getString(R.string.string_path_to_images) +
                ChannelsWorker.getInstance().getChannelBean(position).getImageLink()).into(imageView);
        title.setText(ChannelsWorker.getInstance().getChannelBean(position).getTitle());
        return convertView;
    }

}
