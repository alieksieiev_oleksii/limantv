package ua.com.limantv.instance;


import ua.com.limantv.utils.Utils;

public class YouTubeInstance {

    private static YouTubeInstance youTubeInstance;
    private String youtube_api_key, youtube_video_key;
    private int youtube_video_delay = 1000 * 60;
    private boolean bln_show_at_start, bln_show_with_delay;
    public static YouTubeInstance getInstance(){
        if(youTubeInstance == null){
            youTubeInstance = new YouTubeInstance();
        }
        return youTubeInstance;
    }

    public void setYouTubeInfo(boolean bln_show_at_start, boolean bln_show_with_delay, String youtube_api_key, String youtube_video_key, int youtube_video_delay){
        this.bln_show_at_start = bln_show_at_start;
        this.bln_show_with_delay = bln_show_with_delay;
        this.youtube_api_key = youtube_api_key;
        this.youtube_video_key = youtube_video_key;
        this.youtube_video_delay = youtube_video_delay * 1000 * 60;
    }


    public String getYoutube_api_key() {
        return youtube_api_key;
    }

    public String getYoutube_video_key() {
        return youtube_video_key;
    }

    public int getYoutube_video_delay() {
        return youtube_video_delay;
    }

    public boolean getBln_show_at_start(){
        return bln_show_at_start;
    }

    public boolean getBln_show_with_delay(){
        return bln_show_with_delay;
    }
}
