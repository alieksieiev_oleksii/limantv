package ua.com.limantv.instance;

import java.util.ArrayList;

import ua.com.limantv.models.Channel;


public class ChannelsWorker {

    private static ChannelsWorker instance;
    private ArrayList<Channel> channelBeansArray;

    private ChannelsWorker() {
        channelBeansArray = new ArrayList<Channel>();
    }

    public static void reloadChannelWorker(){
        if(instance != null) {
            instance = null;
            instance = new ChannelsWorker();
        }
    }

    public static ChannelsWorker getInstance() {
        if (instance == null) {
            instance = new ChannelsWorker();
        }
        return instance;
    }

    public void addChannelBean(String channelName, String channelLink, String channelImageLink, String tvProgramLink) {
        channelBeansArray.add(new Channel(channelName, channelLink, channelImageLink, tvProgramLink));
    }

    public Channel getChannelBean(int position) {
        return channelBeansArray.get(position);
    }

    public int getCountChannelsBeans() {
        return channelBeansArray.size();
    }

}
